import HttpRequest from '@/libs/axios'
import Cookies from 'js-cookie';
import config from '@/config'
import Echo from "laravel-echo";

//var tokenCheck='?access-token='+Cookies.get('accessToken');

var docPath = window.document.domain;

if (docPath == 'uat-admin.cjyjx.cn') {
	var gotoUrl = config.baseUrl.pro;
} else if (docPath == 'admin.cjyjx.cn') {
	var gotoUrl = config.baseUrl.proInx;
} else {
	var gotoUrl = config.baseUrl.dev;
}

//广播
window.Pusher = require('pusher-js');

Pusher.logToConsole = true;

window.Echo = new Echo({
	broadcaster: 'pusher',
	key: 'abc123', // 这里随便填，跟.env文件里面一致即可
	wsHost: window.location.hostname,
	wsPort: 6001,
	forceTLS: false,
	disableStats: true,
});


const baseUrl = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : gotoUrl


const axios = new HttpRequest(baseUrl)
export default axios
