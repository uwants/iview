/* eslint-disable */
import Main from '@/components/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */


export default [
	{
		path: '/',
		name: 'login',
		meta: {
			title: 'Login - 登录',
			hideInMenu: true
		},
		component: () => import('@/view/login/login.vue')
	},


	{
		path: '/home',
		name: 'home',
		redirect: '/',
		component: Main,
		meta: {
			hideInMenu: true,
			notCache: true
		},
		children: [
			{
				path: '/home',
				name: 'home',
				meta: {
					hideInMenu: true,
					title: '首页',
					notCache: true,
					icon: 'md-home'
				},
				component: () => import('@/view/single-page/home/home_major.vue')
			}
		]
	},


	//


	//页面路由


	{
		path: '/message',
		name: 'message',
		component: Main,
		meta: {
			hideInBread: true,
			hideInMenu: true
		},
		children: [
			{
				path: 'message_page',
				name: '消息中心',
				meta: {
					icon: 'md-notifications',
					title: '消息中心'
				},
				component: () => import('@/view/single-page/message/index.vue')
			},
			{
				path: 'adminMessage_page',
				name: '个人资料',
				meta: {
					icon: 'md-flower',
					title: '个人资料',
				},
				component: () => import('@/view/single-page/admin/index.vue')
			}
		]
	},


	{
		path: '/401',
		name: 'error_401',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/401.vue')
	},
	{
		path: '/500',
		name: 'error_500',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/500.vue')
	},
	{
		path: '*',
		name: 'error_404',
		meta: {
			hideInMenu: true
		},
		component: () => import('@/view/error-page/404.vue')
	},


	{
		path: '/member',
		name: '用户中心',
		meta: {
			icon: 'md-person',
			title: '用户中心'
		},
		component: Main,
		children: [
			{
				path: '/memberList',
				name: '用户列表',
				meta: {
					icon: '',
					title: '用户列表'
				},
				component: () => import('@/view/member/member.vue'),
			},
			{
				path: 'queryMember',
				name: 'queryMember',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `用户详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/member/member_message.vue')
				//component: () => import('@/view/member/test.vue')
			},


		]
	},

	{
		path: '/company',
		name: '企业中心',
		component: Main,
		meta: {
			icon: 'md-home',
			title: '企业中心'
		},
		children: [
			{
				path: '/companyList',
				name: '企业列表',
				meta: {
					icon: '',
					title: '企业列表'
				},
				component: () => import('@/view/company/company_list.vue')
			},
			{
				path: 'queryCompany',
				name: 'queryCompany',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `企业详情 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/company/company_message.vue')
				//component: () => import('@/view/member/test.vue')
			},
			{
				path: 'editCompany',
				name: 'editCompany',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `企业编辑 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/company/company_edit.vue')
				//component: () => import('@/view/member/test.vue')
			},
			{
				path: '/companyExamine',
				name: '认证审核',
				meta: {
					icon: '',
					title: '认证审核'
				},
				component: () => import('@/view/companyExamine/list.vue')
			},
			{
				path: 'examineCompany',
				name: 'examineCompany',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `认领审核 - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/companyExamine/message.vue')
			},
			{
				path: '/companyHot',
				name: '热门企业',
				meta: {
					icon: '',
					title: '热门企业'
				},
				component: () => import('@/view/companyHot/list.vue')
			},
		]
	},
	{
		path: '/company',
		name: '意见中心',
		component: Main,
		meta: {
			icon: 'ios-albums',
			title: '意见中心'
		},
		children: [
			{
				path: '/commentList',
				name: '意见列表',
				meta: {
					icon: '',
					title: '意见列表'
				},
				component: () => import('@/view/comment/comment_list.vue')
			},
			{
				path: 'queryComment',
				name: 'queryComment',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/comment/comment_message.vue')
			},
			{
				path: '/repliesList',
				name: 'replies_list',
				meta: {
					icon: '',
					title: '意见回复'
				},
				component: () => import('@/view/replies/replies_list')
			},
			{
				path: 'queryReply',
				name: 'queryReply',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/replies/replies_message')
			},
			{
				path: '/solicitationList',
				name: '意见箱列表',
				meta: {
					icon: '',
					title: '意见箱列表'
				},
				component: () => import('@/view/solicitation/solicitation_list.vue')
			},
			{
				path: 'querySolicitation',
				name: 'querySolicitation',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/solicitation/solicitation_message.vue')
			},
			{
				path: 'querySolicitationList',
				name: 'querySolicitationList',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/solicitation/solicitation_comment_list.vue')
			},
		]
	},
	{
		path: '/service',
		name: '服务中心',
		component: Main,
		meta: {
			icon: 'logo-freebsd-devil',
			title: '服务中心'
		},
		children: [
			{
				path: '/weixinTemplate',
				name: '微信消息模板',
				meta: {
					icon: '',
					title: '微信消息模板'
				},
				component: () => import('@/view/weixinTemplate/list.vue')
			},
			{
				path: '/messageTemplate',
				name: 'queryTemplate',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `模板消息 - ${route.query.name}`,
					notCache: true
				},
				component: () => import('@/view/weixinTemplate/send_message.vue')
			},
			{
				path: '/customMenu',
				name: '自定义菜单',
				meta: {
					icon: '',
					title: '自定义菜单'
				},
				component: () => import('@/view/customMenu/message.vue')
			},
		]
	},
	{
		path: '/data',
		name: '数据中心',
		component: Main,
		meta: {
			icon: 'md-stats',
			title: '数据中心'
		},
		children: [
			{
				path: '/userData',
				name: '用户数据',
				meta: {
					icon: '',
					title: '用户数据'
				},
				component: () => import('@/view/data/user-data.vue')
			},
			{
				path: '/everyData',
				name: '每日数据',
				meta: {
					icon: '',
					title: '每日数据'
				},
				component: () => import('@/view/data/every-data.vue')
			},
			{
				path: '/activityData',
				name: '活动数据',
				meta: {
					icon: '',
					title: '活动数据'
				},
				component: () => import('@/view/data/activity-data.vue')
			}

		]
	},
	{
		path: '/applets',
		name: '小程序',
		component: Main,
		meta: {
			icon: 'logo-codepen',
			title: '小程序'
		},
		children: [
			{
				path: '/applatsEdition',
				name: '小程序版本管理',
				meta: {
					icon: '',
					title: '小程序版本管理'
				},
				component: () => import('@/view/applatsEdition/message.vue')
			},
			{
				path: 'queryEdition',
				name: 'queryEdition',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `版本 - ${route.query.name}`,
					notCache: true
				},
				component: () => import('@/view/applatsEdition/message_edit.vue')
			},
			{
				path: 'article',
				name: '文章管理',
				meta: {
					icon: '',
					title: '文章管理'
				},
				component: () => import('@/view/applatsArticle/article.vue')
			},
			{
				path: 'queryArticle',
				name: 'queryArticle',
				meta: {
					hideInMenu: true,
					icon: 'md-flower',
					title: route => `${route.query.name} - ${route.query.id}`,
					notCache: true
				},
				component: () => import('@/view/applatsArticle/article_message.vue')
			},
			{
				path: '/applatsKeyword',
				name: '关键词管理',
				meta: {
					icon: '',
					title: '关键词管理'
				},
				component: () => import('@/view/applatsKeyword/message.vue')
			},
			{
				path: '/applatsFaq',
				name: 'FAQ管理',
				meta: {
					icon: '',
					title: 'FAQ管理'
				},
				component: () => import('@/view/applatsFaq/FaqList.vue')
			},
			{
				path: '/applatsBanner',
				name: 'Banner管理',
				meta: {
					icon: '',
					title: 'Banner管理'
				},
				component: () => import('@/view/applatsBanner/BannerList.vue')
			},
		]
	},
	{
		path: '/Operate',
		name: '运营中心',
		component: Main,
		meta: {
			icon: 'logo-snapchat',
			title: '运营中心'
		},
		children: [
			{
				path: 'activity',
				name: '活动管理',
				meta: {
					icon: '',
					title: '活动管理',
					refresh: false,
				},
				component: () => import('@/view/operate/activity.vue')
			},
		]
	},

	{
		path: '/platform',
		name: '多端管理',
		component: Main,
		meta: {
			icon: 'logo-codepen',
			title: '多平台'
		},
		children: [
			{
				path: '/platformBanner',
				name: 'platformBanner管理',
				meta: {
					icon: 'md-arrow-dropright',
					title: 'Banner管理'
				},
				component: () => import('@/view/platformBanner/platformBannerList.vue')
			},
		]
	},
	{
		path: '/Administration',
		name: '系统管理',
		component: Main,
		meta: {
			icon: 'md-cog',
			title: '系统管理'
		},
		children: [
			{
				path: 'admin',
				name: '管理员列表',
				meta: {
					icon: '',
					title: '管理员列表',
					refresh: false,
				},
				component: () => import('@/view/Administration/admin.vue')
			},
			{
				path: 'role',
				name: '角色列表',
				meta: {
					icon: '',
					title: '角色列表'
				},
				component: () => import('@/view/Administration/role.vue')
			},
			{
				path: 'adminMenu',
				name: '菜单列表',
				meta: {
					icon: 't',
					title: '菜单列表',
					refresh: false,
				},
				component: () => import('@/view/Administration/menu.vue')
			},
			{
				path: 'testUser',
				name: 'testUser',
				meta: {
					icon: '',
					title: '测试用户'
				},
				component: () => import('@/view/testUser/testUser.vue')
			},
		]
	},

	{
		path: '/custom',
		name: '自定义',
		component: Main,
		meta: {
			icon: 'logo-github',
			title: '自定义'
		},
		children: [
			{
				path: 'customPage',
				name: '小程序首页设置',
				meta: {
					icon: '',
					title: '小程序首页设置',
					refresh: false,
				},
				component: () => import('@/view/applatsHome/message.vue')
			},
			{
				path: '/applatsSelf',
				name: '个人中心',
				meta: {
					icon: 'md-arrow-dropright',
					title: '个人中心'
				},
				component: () => import('@/view/applatsSelf/message.vue')
			},
		]
	},
	{
		path: '/components',
		name: 'components',
		meta: {
			icon: 'logo-buffer',
			title: '组件'
		},
		component: Main,
		children: [
			{
				path: 'tree_select_page',
				name: 'tree_select_page',
				meta: {
					icon: 'md-arrow-dropdown-circle',
					title: '树状下拉选择器'
				},
				component: () => import('@/view/components/tree-select/index.vue')
			},
			{
				path: 'count_to_page',
				name: 'count_to_page',
				meta: {
					icon: 'md-trending-up',
					title: '数字渐变'
				},
				component: () => import('@/view/components/count-to/count-to.vue')
			},
			{
				path: 'drag_list_page',
				name: 'drag_list_page',
				meta: {
					icon: 'ios-infinite',
					title: '拖拽列表'
				},
				component: () => import('@/view/components/drag-list/drag-list.vue')
			},
			{
				path: 'drag_drawer_page',
				name: 'drag_drawer_page',
				meta: {
					icon: 'md-list',
					title: '可拖拽抽屉'
				},
				component: () => import('@/view/components/drag-drawer')
			},
			{
				path: 'org_tree_page',
				name: 'org_tree_page',
				meta: {
					icon: 'ios-people',
					title: '组织结构树'
				},
				component: () => import('@/view/components/org-tree')
			},
			{
				path: 'tree_table_page',
				name: 'tree_table_page',
				meta: {
					icon: 'md-git-branch',
					title: '树状表格'
				},
				component: () => import('@/view/components/tree-table/index.vue')
			},
			{
				path: 'cropper_page',
				name: 'cropper_page',
				meta: {
					icon: 'md-crop',
					title: '图片裁剪'
				},
				component: () => import('@/view/components/cropper/cropper.vue')
			},
			{
				path: 'tables_page',
				name: 'tables_page',
				meta: {
					icon: 'md-grid',
					title: '多功能表格'
				},
				component: () => import('@/view/components/tables/tables.vue')
			},
			{
				path: 'split_pane_page',
				name: 'split_pane_page',
				meta: {
					icon: 'md-pause',
					title: '分割窗口'
				},
				component: () => import('@/view/components/split-pane/split-pane.vue')
			},
			{
				path: 'markdown_page',
				name: 'markdown_page',
				meta: {
					icon: 'logo-markdown',
					title: 'Markdown编辑器'
				},
				component: () => import('@/view/components/markdown/markdown.vue')
			},
			{
				path: 'editor_page',
				name: 'editor_page',
				meta: {
					icon: 'ios-create',
					title: '富文本编辑器'
				},
				component: () => import('@/view/components/editor/editor.vue')
			},
			{
				path: 'icons_page',
				name: 'icons_page',
				meta: {
					icon: '_bear',
					title: '自定义图标'
				},
				component: () => import('@/view/components/icons/icons.vue')
			}
		]
	},
	{
		path: '/update',
		name: 'update',
		meta: {
			icon: 'md-cloud-upload',
			title: '数据上传'
		},
		component: Main,
		children: [
			{
				path: 'update_table_page',
				name: 'update_table_page',
				meta: {
					icon: 'ios-document',
					title: '上传Csv'
				},
				component: () => import('@/view/update/update-table.vue')
			},
			{
				path: 'update_paste_page',
				name: 'update_paste_page',
				meta: {
					icon: 'md-clipboard',
					title: '粘贴表格数据'
				},
				component: () => import('@/view/update/update-paste.vue')
			}
		]
	},
	{
		path: '/excel',
		name: 'excel',
		meta: {
			icon: 'ios-stats',
			title: 'EXCEL导入导出'
		},
		component: Main,
		children: [
			{
				path: 'upload-excel',
				name: 'upload-excel',
				meta: {
					icon: 'md-add',
					title: '导入EXCEL'
				},
				component: () => import('@/view/excel/upload-excel.vue')
			},
			{
				path: 'export-excel',
				name: 'export-excel',
				meta: {
					icon: 'md-download',
					title: '导出EXCEL'
				},
				component: () => import('@/view/excel/export-excel.vue')
			}
		]
	},

]

